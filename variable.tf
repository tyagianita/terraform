variable "aws_region" {
  default = "us-east-1"
}

variable "pub_key_path" {
  default = "~/.ssh/id_rsa.pub"
}

variable "vpc_name" {
  default = "PROD"
}

variable "vpc_cidr" {
  default = "192.0.0.0/16"
}
variable "number_of_instances" {
  default = 2
}

variable "route53_zone_name" {
  default = "internalprod.drinsta.com"
}

variable "mgmt_subnet_aza_cidr" {
  default = "192.0.1.0/24"
}

variable "mgmt_subnet_azb_cidr" {
  default = "192.0.2.0/24"
}

variable "app_subnet_aza_cidr" {
  default = "192.0.3.0/24"
}

variable "app_subnet_azb_cidr" {
  default = "192.0.4.0/24"
}

variable "db_subnet_aza_cidr" {
  default = "192.0.5.0/24"
}

variable "db_subnet_azb_cidr" {
  default = "192.0.6.0/24"
}

