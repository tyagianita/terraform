output "mgmt_subnet_aza_id" {
  value = "${module.mgmt_subnet_aza.id}"
}

output "mgmt_subnet_azb_id" {
  value = "${module.mgmt_subnet_azb.id}"
}

output "app_subnet_aza_id" {
  value = "${module.app_subnet_aza.id}"
}

output "app_subnet_azb_id" {
  value = "${module.app_subnet_azb.id}"
}


output "key_name" {
  value = "${var.vpc_name}-key"
}

output "zone_id" {
  value = "${module.vpc.zone_id}"
}

output "default_sg_id" {
  value = "${module.vpc.default_sg_id}"
}

output "pub_http_sg_id" {
  value = "${module.pub_http_sg.id}"
}

